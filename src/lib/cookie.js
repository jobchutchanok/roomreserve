const setCookie = (name, value, options = {}) => {
    if (typeof document === 'undefined') {
        return; // Do nothing if running in a non-browser environment
    }

    options = {
        path: '/',
        ...options
    };

    let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += '; ' + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += '=' + optionValue;
        }
    }

    document.cookie = updatedCookie;
};

const removeCookie = async (name) => {
    return new Promise((resolve, reject) => {
        document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
        resolve(); // Resolve the promise once the cookie is removed
    });
};

const getCookieValue = (name) => {
    const cookies = document.cookie.split('; ');
    for (const cookie of cookies) {
        const [cookieName, cookieValue] = cookie.split('=');
        if (cookieName === name) {
            return cookieValue;
        }
    }
    return null;
};

export { setCookie, removeCookie, getCookieValue }