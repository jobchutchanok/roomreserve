import React, { useState } from 'react';
import { Container, Grid, Table, TableBody, TableCell, TableHead, TableRow, Button, CardContent, Select, MenuItem } from '@mui/material';
import generateTimeOptions, { calculateBookingDates, isTimeDisabled } from '../helper/time';
import { ReserveModule } from './ReserveModule';
import useEventStore from '../store/storeOfevent';
import useReserve from '../hooks/useReserve';
import { LoadingSpinner } from './LoadingSpinner';

const TimeSchedule = ({ data }) => {
    const [pickedTimes, setPickedTimes] = useState([]);
    const [timeOptions, setTimeOptions] = useState(generateTimeOptions());
    const [open, setOpen] = useState(false)
    const events = useEventStore((state) => state.events)
    const setEvent = useEventStore((state) => state.setEvent)
    // if (error) return null;

    // if (isLoading) return <LoadingSpinner />;
    const handlerAddEvent = async () => {
        await setEvent(data)
        console.log(events)
    }


    const handleOpen = () => setOpen(true);

    const handleClose = () => setOpen(false);
    // console.log(first)
    const addTime = (time) => {
        if (!pickedTimes.includes(time)) {
            setPickedTimes([...pickedTimes, time].sort((a, b) => {
                // Convert times to Date objects for comparison
                const timeA = new Date(`1970/01/01 ${a}`);
                const timeB = new Date(`1970/01/01 ${b}`);
                return timeA - timeB;
            }));
            setTimeOptions(timeOptions.filter(option => option !== time));
        }
    };

    const removeTime = (time) => {
        const updatedTimes = pickedTimes.filter((pickedTime) => pickedTime !== time);
        setPickedTimes(updatedTimes);
        setTimeOptions([...timeOptions, time].sort((a, b) => {
            // Convert times to Date objects for comparison
            const timeA = new Date(`1970/01/01 ${a}`);
            const timeB = new Date(`1970/01/01 ${b}`);
            return timeA - timeB;
        }));
    };
    // Example usage:
    const startDate = new Date('2024-04-05'); // Start date of the booking
    const repeatType = 'week'; // Repeat type: 'day', 'week', or 'month'
    const repeatEndDate = '2024-05-31'; // End date of the repeating bookings (optional)
    const repeatDays = [1, 2, 3, 4, 5]; // For weekly repeats (Monday to Friday)
    const beginTime = '09:00'; // Begin time of the booking
    const endTime = '17:00'; // End time of the booking

    const bookingDates = calculateBookingDates(startDate, repeatType, repeatEndDate, repeatDays, beginTime, endTime);
    console.log('bookingDates', bookingDates); // Array of booking dates based on repeat options
    return (
        <Container>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                    <h3>Time Schedule</h3>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Time Options</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableCell>{'ชื่อผู้จอง'}</TableCell>
                            <TableCell>{'เวลา'}</TableCell>
                            {/* <TableCell>
                                <Button variant="contained" onClick={() => addTime(timeOption)}>Pick</Button>
                            </TableCell> */}
                            {/* <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                // value={selectedGuestCount}
                                label="Number of Adults"
                                sx={{ width: '100%' }}
                            // onChange={handleChange}
                            >
                                {timeOptions?.map((guest) => (
                                    <MenuItem key={guest} value={guest} disabled={isTimeDisabled(guest, beginTime, endTime)}>
                                        {guest}
                                    </MenuItem>
                                ))}
                            </Select> */}
                            {/* {timeOptions.map((timeOption, index) => (
                                <TableRow key={index}>
                                    <TableCell>{timeOption}</TableCell>
                                    <TableCell>{ }</TableCell>
                                    <TableCell>
                                        <Button variant="contained" onClick={() => addTime(timeOption)}>Pick</Button>
                                    </TableCell>
                                </TableRow>
                            ))} */}
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <h3>Reserve Detail</h3>
                    <CardContent>
                        <ReserveModule room={data} open={open} handleClose={handleClose} />
                    </CardContent>
                </Grid>
            </Grid>
        </Container>
    );
};

export default TimeSchedule;
