import React, { useEffect, useContext } from 'react';
import { Route, useNavigate } from 'react-router-dom';
import { getCookieValue } from '../lib/cookie';
// import {auth} from '../lib/firebase'


const PrivateRoute = ({ component: Component }) => {
  const navigate = useNavigate();
  const cookieValue = getCookieValue('token')
  useEffect(() => {
    if (!cookieValue) {
      navigate('/'); // Redirect to '/' if currentUser is not available
    }
    // auth.onAuthStateChanged(user=>{
    //   if(!user){
    //     navigate('/')
    //   }
    // })
  }, []);

  return (
    <Component />
  )
}

export default PrivateRoute;