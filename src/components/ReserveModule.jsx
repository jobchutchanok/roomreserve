import React, { useCallback, useEffect, useState } from "react";
import "react-date-range/dist/styles.css"; //
import "react-date-range/dist/theme/default.css";
import {
    Modal,
    Typography,
    Box,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Button,
    FormControlLabel,
    RadioGroup,
    Radio,
    FormLabel
} from "@mui/material";
import { DateRange } from "react-date-range";
import { getDate } from "date-fns";
// import { AuthContext } from "../context/AuthContext";
import { addDoc, collection } from "firebase/firestore";
// import { db } from "../lib/firebase";
import { LoadingSpinner } from "./LoadingSpinner";
import { toast } from "react-hot-toast";
import { bookModalStyle } from "../helper/styles";
import { useNavigate } from "react-router-dom";
import { Toggle } from 'rsuite';
// import { DatePicker } from 'rsuite';
import { DatePicker, Stack } from 'rsuite';
import generateTimeOptions from "../helper/time";

export const ReserveModule = ({ open, handleClose, room }) => {
    // const { currentUser } = useContext(AuthContext);
    const navigate = useNavigate()
    const [timeOptions, setTimeOptions] = useState(generateTimeOptions());
    const [guests, setGuests] = useState();
    const [selectedGuestCount, setSelectedGuestCount] = useState(1);
    const [dates, setDates] = useState([
        {
            startDate: new Date(),
            endDate: null,
            key: "selection",
        },
    ]);
    const [reserveObj, setReserveObj] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const handleChange = (event) => {
        setSelectedGuestCount(event.target.value);

    };

    function numberOfGuests(maxGuest) {
        const guestsArr = [];

        for (let i = 1; i <= maxGuest; i++) {
            guestsArr.push(i);
        }
        return guestsArr;
    }

    useEffect(() => {
        // setGuests(numberOfGuests(room?.rooms[0]?.content?.split(" ")[0]));
    }, [room]);

    function getTotalNightsBooked() {
        const startDate = getDate(dates[0].startDate);
        const endDate = getDate(dates[0].endDate);
        const totalnightsBooked = endDate - startDate;
        return totalnightsBooked;
    }
    const updateFormValue = useCallback(({ updateType, value }) => {
        setReserveObj(prevReserveObj => ({
            booking: {
                ...prevReserveObj.booking,
                [updateType]: value,
            }
        }));
    }, [setReserveObj]);

    getTotalNightsBooked();

    // const bookings = collection(db, "bookings");

    const handleReserve = async () => {
        setIsLoading(true);
        // const { uid, displayName } = currentUser;
        // await addDoc(bookings, {
        //   hotelAddress: room.address,
        //   hotelName: room.name,
        //   numberOfGuests: selectedGuestCount,
        //   bookingStartDate: `${dates[0].startDate}`,
        //   bookingEndDate: `${dates[0].endDate}`,
        //   price: room?.pricePerNight * getTotalNightsBooked(),
        //   bookedBy: {
        //     uid,
        //     displayName,
        //   },
        // })
        //   .then(() => {
        //     toast.success("booking successfull");
        //     handleClose();
        //     navigate('/my-profile')
        //     setIsLoading(false);
        //   })
        //   .catch((error) => {
        //     toast.error(error);
        //     setIsLoading(false);
        //   });
    };

    return (
        <Box >
            <Stack spacing={10} direction="column" alignItems="flex-start">
                <label>Controlled Value:</label>
                <DatePicker />

                <label>Uncontrolled Value:</label>
                <DatePicker defaultValue={new Date()} />
            </Stack>
            <Box sx={{ position: 'relative', marginTop: 2 }}>
                <InputLabel>Select Dates</InputLabel>
            </Box>
            <FormLabel id="demo-row-radio-buttons-group-label">Repeat</FormLabel>
            <Toggle />
            <FormControl fullWidth sx={{ marginTop: 3 }}>
                <InputLabel id="demo-simple-select-label">
                    StartTime
                </InputLabel>
                <Select
                    name="beginTime"
                    id="demo-simple-select"
                    // value={selectedGuestCount}
                    label="Number of Adults"
                    sx={{ width: '100%' }}
                // onChange={handleChange}
                >
                    {timeOptions?.map((time) => (
                        <MenuItem key={time} value={time} >
                            {time}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl fullWidth sx={{ marginTop: 3 }}>
                <InputLabel id="demo-simple-select-label">
                    EndTime
                </InputLabel>
                <Select
                    name="endTime"
                    // value={selectedGuestCount}
                    label="Number of Adults"
                    sx={{ width: '100%' }}
                // onChange={handleChange}
                >
                    {timeOptions?.map((time) => (
                        <MenuItem key={time} value={time} >
                            {time}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>

            <Button
                onClick={handleReserve}
                sx={{ width: "100%", marginTop: 2 }}
                variant="outlined"
                color="primary"
                disabled={!dates[0].endDate}
            >
                {isLoading ? (
                    <LoadingSpinner color={"primary"} size={20} />
                ) : (
                    "Reserve"
                )}
            </Button>
        </Box>

    );
};
