import { Calendar, Badge } from 'rsuite';
// (Optional) Import component styles. If you are using Less, import the `index.less` file. 

function getTodoList(date) {
    const day = date.getDate();
    switch (day) {
        case 10:
            return [
                { time: '10:30 am', title: 'Meeting' },
                { time: '12:00 pm', title: 'Lunch' }
            ];
        case 15:
            return [
                { time: '09:30 pm', title: 'Products Introduction Meeting' },
                { time: '12:30 pm', title: 'Client entertaining' },
                { time: '02:00 pm', title: 'Product design discussion' },
                { time: '05:00 pm', title: 'Product test and acceptance' },
                { time: '06:30 pm', title: 'Reporting' },
                { time: '10:00 pm', title: 'Going home to walk the dog' }
            ];
        default:
            return [];
    }
}
export const CalenDar = () => {
    function renderCell(date) {
        const list = getTodoList(date);

        if (list.length) {
            return (
                <>
                    <Badge />
                </>
            );
        }

        return null;
    }
    function handleSelect(date) {
        // You can perform any action here based on the selected date
        console.log('Selected Date:', date);
    }

    return (
        <>
            <Calendar compact bordered renderCell={renderCell} onSelect={handleSelect} />
            {/* <button onClick={increasePopulation(['test1', 'test2'])}>test</button> */}
        </>
    );
}