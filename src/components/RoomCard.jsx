import React from 'react'
import { Card, CardContent, CardMedia, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom';
import defaultPicture from '../assets/meeting-rooms-4.jpg'


export const RoomCard = ({ room }) => {
    const navigate = useNavigate()
    return (
        <Card   >
            {/* <CardMedia
                sx={{ objectFit: 'cover' }}
                component="img"
                height="244"
                image={defaultPicture}
                alt="Paella dish"
                loading='lazy'
            /> */}
            <CardContent>
                <Typography sx={{ cursor: "pointer" }} onClick={() => navigate(`/rooms/${room.slug}`)} >{room.name}</Typography>
                <Typography marginTop={1} fontSize={14}>{room.room_type} </Typography>

            </CardContent>

        </Card>
    )
}
