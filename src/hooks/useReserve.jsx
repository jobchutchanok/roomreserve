import { useQuery } from "@tanstack/react-query";
import APIReserve from "../services/get-reserve"

const reserveAPI = new APIReserve('requests');

const useReserve = () => {
    return useQuery({
        queryKey: ["reserveList"],
        queryFn: reserveAPI.getAll,
        staleTime: 5000,
        // initialData: genres,
    });
};

export default useReserve;