import { useQuery, useMutation } from "@tanstack/react-query";
import APILogin from "../services/get-login"

const apiLogin = new APILogin('/login');

const useLogin = () => {
    const { mutateAsync: loginFn } = useMutation({
        mutationFn: apiLogin.loginUser
    })
    return loginFn
};

// const useLogin = (userData) => { // Accept userData as a parameter
//     return useQuery({
//         queryKey: ["login"],
//         queryFn: () => apiLogin.loginUser(userData), // Pass userData to loginUser function
//         staleTime: 5000,
//     });
// };


export default useLogin;