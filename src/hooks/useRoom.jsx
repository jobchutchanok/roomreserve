import { useQuery } from "@tanstack/react-query";
import APIRooms from "../services/get-rooms"

const roomsAPI = new APIRooms('rooms');

const useRooms = () => {
    const getAllRooms = () => {
        return useQuery({
            queryKey: ["rooms"],
            queryFn: roomsAPI.getAll,
            staleTime: 5000,
            // initialData: genres,
        });
    };

    const getRoomBySlug = (slug) => {
        return useQuery({
            queryKey: ["rooms", slug],
            queryFn: () => roomsAPI.getRoomByslug(slug),
            staleTime: 5000,
            // initialData: genres,
        });
    };
    return { getAllRooms, getRoomBySlug };
};

export default useRooms;