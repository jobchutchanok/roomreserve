import React, { useState, useEffect } from "react";
import { Container, Button, Typography, TextField, Box } from "@mui/material";
import { signInWithPopup } from "firebase/auth";
// import { auth, provider, db } from "../lib/firebase";
import { doc, setDoc } from "firebase/firestore";
import { useLocation, useNavigate } from "react-router-dom";
import { toast, Toaster } from "react-hot-toast";
import useLogin from "../hooks/useLogin";
import { setCookie } from "../lib/cookie";

export default function Login() {
    const navigate = useNavigate();
    const location = useLocation();
    const [userData, setUserData] = useState({ username: '', password: '' });
    const mutateFn = useLogin();
    const [checkedCookie, setCheckedCookie] = useState(false);
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setUserData((prevUserData) => ({
            ...prevUserData,
            [name]: value,
        }));
    };
    const handleLogin = async () => {
        try {
            console.log(userData)
            // Call the refetch function to trigger the login process again
            await mutateFn(userData);
        } catch (error) {
            console.error('Login error:', error);
            // Handle error
        }
    };
    useEffect(() => {
        const queryParams = new URLSearchParams(location.search);
        const token = queryParams.get("token");
        if (token) {
            // Set the cookie with the token value and a 4-hour expiry time
            const expiryDate = new Date();
            expiryDate.setTime(expiryDate.getTime() + 4 * 60 * 60 * 1000); // 4 hours
            setCookie("token", token, { expires: expiryDate });
            // Navigate to the desired page
            navigate("/rooms");
        }
    }, [location.search, navigate]);
    // const handleLogin = async () => {
    //     try {
    //         if (!username || !password) {
    //             return toast.error('Empty username,password');
    //         }
    //         let userData = {
    //             username: username,
    //             password: password
    //         }
    //         const loginResponse = await login(userData);;
    //         console.log('Login successful:', loginResponse);

    //         // if (user && user.password === password) {
    //         //     const expirationTime = new Date();
    //         //     expirationTime.setTime(expirationTime.getTime() + (4 * 60 * 60 * 1000)); // 1 hour in milliseconds
    //         //     document.cookie = `username=${username}; expires=${expirationTime.toUTCString()}; path=/`;
    //         //     toast.success('Login successful');
    //         //     setCheckedCookie(true)
    //         // } else {
    //         //     toast.error('Invalid username or password');
    //         // }
    //     } catch (error) {
    //         console.log(error)
    //         // toast.error(errorMessage);
    //     }
    //     // signInWithPopup(auth, provider)
    //     //   .then(async (result) => {a
    //     //     const user = result.user;
    //     //     if (user) {
    //     //       await setDoc(doc(db, "users", user.uid), {
    //     //         uid: user.uid,
    //     //         displayName: user.displayName,
    //     //         photoURL: user.photoURL,
    //     //       });
    //     //     }
    //     //   })
    //     //   .catch((error) => {
    //     //     const errorMessage = error.message;

    //     //     toast.error(errorMessage);
    //     //   });
    // };

    return (
        <>
            <Container
                sx={{ display: "grid", placeContent: "center", height: "100vh" }}
                maxWidth="md"
            >
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '100vh',
                    }}
                >
                    <Typography textAlign={"center"} variant="h4" sx={{ marginBottom: 4 }}>
                        Welcome to BookStay
                    </Typography>
                    <Box sx={{ width: 300 }}>
                        <TextField
                            fullWidth
                            label="Username"
                            name="username"
                            variant="outlined"
                            margin="normal"
                            value={userData.username}
                            onChange={handleInputChange}
                        />
                        <TextField
                            fullWidth
                            label="Password"
                            name="password"
                            type="password"
                            variant="outlined"
                            margin="normal"
                            value={userData.password}
                            onChange={handleInputChange}
                        />
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={handleLogin}
                            sx={{
                                mt: 2,
                                py: 2, // Padding on the y-axis
                                borderRadius: 0, // Square edges
                                '&:hover': {
                                    backgroundColor: '#1976d2', // Darken background color on hover
                                },
                            }}
                        >
                            Login
                        </Button>
                        <Button
                            fullWidth
                            variant="outlined"
                            color="primary"
                            // onClick={handleLogin}
                            sx={{
                                mt: 1, // Slightly less margin than the primary button
                                py: 2, // Padding on the y-axis
                                borderRadius: 0, // Square edges
                                border: '2px solid #1976d2', // Custom border style
                                '&:hover': {
                                    backgroundColor: '#e3f2fd', // Lighten background color on hover
                                },
                            }}
                        >
                            Login with Google
                        </Button>
                    </Box>
                </Box>


            </Container>
            <Toaster
                position="top-right"
                toastOptions={{
                    duration: 1500,
                    style: {
                        fontSize: 14,
                    },
                }}
            />
        </>
    );
}
