import { addDays, addWeeks, addMonths, isMonday, isFriday, isSaturday, isSunday } from 'date-fns';
export default function generateTimeOptions() {
    const timeOptions = [];
    for (let hour = 7; hour <= 21; hour++) {
        for (let minute = 0; minute < 60; minute += 30) {
            const formattedHour = hour.toString().padStart(2, '0');
            const formattedMinute = minute.toString().padStart(2, '0');
            const time = `${formattedHour}:${formattedMinute}`;
            timeOptions.push(time);
        }
    }
    return timeOptions;
}

export function isTimeDisabled(time, begin, end) {
    const [beginHour, beginMinute] = begin.split(":").map(Number);
    const [endHour, endMinute] = end.split(":").map(Number);
    const [hour, minute] = time.split(":").map(Number);

    const isAfterBegin = hour > beginHour || (hour === beginHour && minute >= beginMinute);
    const isBeforeEnd = hour < endHour || (hour === endHour && minute <= endMinute);

    return isAfterBegin && isBeforeEnd;
}

export function calculateBookingDates(startDate, repeatType, repeatEndDate, repeatDays, beginTime, endTime) {
    const bookingDates = [];

    let currentDate = new Date(startDate);
    let endDate = repeatEndDate ? new Date(repeatEndDate) : null;

    while (!endDate || currentDate <= endDate) {
        // Check if the current date matches the selected days for weekly repeats
        if (repeatType === 'week' && repeatDays.includes(currentDate.getDay()) && !isSaturday(currentDate) && !isSunday(currentDate)) {
            const bookingStartDateTime = new Date(currentDate);
            bookingStartDateTime.setHours(beginTime.split(':')[0]);
            bookingStartDateTime.setMinutes(beginTime.split(':')[1]);

            const bookingEndDateTime = new Date(currentDate);
            bookingEndDateTime.setHours(endTime.split(':')[0]);
            bookingEndDateTime.setMinutes(endTime.split(':')[1]);

            bookingDates.push({ start: bookingStartDateTime, end: bookingEndDateTime });
        }
        // Check if the current date is a weekday for monthly repeats
        else if (repeatType === 'month' && repeatDays.includes(currentDate.getDate()) && !isSaturday(currentDate) && !isSunday(currentDate)) {
            const bookingStartDateTime = new Date(currentDate);
            bookingStartDateTime.setHours(beginTime.split(':')[0]);
            bookingStartDateTime.setMinutes(beginTime.split(':')[1]);

            const bookingEndDateTime = new Date(currentDate);
            bookingEndDateTime.setHours(endTime.split(':')[0]);
            bookingEndDateTime.setMinutes(endTime.split(':')[1]);

            bookingDates.push({ start: bookingStartDateTime, end: bookingEndDateTime });
        }
        // For other repeat types (e.g., 'day'), add the date as is
        else if (repeatType === 'day') {
            const bookingStartDateTime = new Date(currentDate);
            bookingStartDateTime.setHours(beginTime.split(':')[0]);
            bookingStartDateTime.setMinutes(beginTime.split(':')[1]);

            const bookingEndDateTime = new Date(currentDate);
            bookingEndDateTime.setHours(endTime.split(':')[0]);
            bookingEndDateTime.setMinutes(endTime.split(':')[1]);

            bookingDates.push({ start: bookingStartDateTime, end: bookingEndDateTime });
        }

        // Increment the current date based on the repeat type
        switch (repeatType) {
            case 'day':
                currentDate = addDays(currentDate, 1);
                break;
            case 'week':
                currentDate = addWeeks(currentDate, 1);
                break;
            case 'month':
                currentDate = addMonths(currentDate, 1);
                break;
            default:
                break;
        }
    }

    return bookingDates;
}

