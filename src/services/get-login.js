import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  timeout: 5000,
  // params: {
  //     key: import.meta.env.VITE_API_KEY,
  // },
});

class APILogin {
  constructor(endpoint) {
    this.endpoint = endpoint;
  }
  loginUser = async (userData) => {
    const res = await axiosInstance.get(this.endpoint, {
      params: userData // userData should be an object containing query parameters
    });
    return res.data;
  }
}

export default APILogin;