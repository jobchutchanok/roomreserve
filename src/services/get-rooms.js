import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    timeout: 5000,
    // params: {
    //     key: import.meta.env.VITE_API_KEY,
    // },
});

class APIRooms {
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    getAll = async () => {
        const res = await axiosInstance.get(`${this.endpoint}`);
        return res.data;
    }


    getRoomByslug = async (slug) => {
        const res = await axiosInstance.get(this.endpoint + "/" + slug);
        return res.data;
    };
}

export default APIRooms;