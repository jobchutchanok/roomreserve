import noImage from "../assets/meeting-rooms-4.jpg";

const getCroppedImageUrl = (url) => {
    if (!url) return noImage;

    const target = "media/";
    const insert_string = "crop/600/400/";
    const insert_pos = url.indexOf(target) + target.length;
    return (
        url.substring(0, insert_pos) + insert_string + url.substring(insert_pos)
    );
};

export default getCroppedImageUrl;