import { createContext, useEffect, useState } from "react";
// import { auth } from "../lib/firebase";
import { getCookieValue } from "../lib/cookie";
export const AuthContext = createContext({});

export const AuthContextProvider = ({ children, setDarkMode }) => {
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    const cookieValue = getCookieValue('token');
    if (cookieValue) {
      setCurrentUser(cookieValue);
    } else {
      setCurrentUser(null); // No username cookie found
    }
    // const unsubscribe = auth.onAuthStateChanged((user) => {
    //   if (user) {
    //     setCurrentUser(user);
    //   }
    // });
    // return () => unsubscribe();
  }, []);

  return (
    <AuthContext.Provider value={{ currentUser, setDarkMode }}>
      {children}
    </AuthContext.Provider>
  );
};
