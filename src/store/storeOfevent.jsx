import { create } from 'zustand'

const useEventStore = create((set) => ({
    events: {},
    setEvent: (newEvent) =>
        set((state) => ({ events: { ...state.events, ...newEvent } })),
}))

export default useEventStore;